import cv2 
import os 
import numpy as np 

car_cascade = cv2.CascadeClassifier('C:/Users/mateu/Desktop/Haarcascade/data/cascade.xml')

num_img = 1

for i in range(4000):
    
    img = cv2.imread('C:/Users/mateu/Desktop/Haarcascade/Raw images/Test/cars_test/1 ('+str(num_img)+').jpg', cv2.IMREAD_GRAYSCALE)
    
    img2 = cv2.resize(img,(100,100))
    
    car = car_cascade.detectMultiScale(img2, 1.1, 1)
        
    for (x,y,w,h) in car:
       
        cv2.rectangle(img2,(x,y),(x+w,y+h),(255,255,0),2)
        
    cv2.imwrite('C:/Users/mateu/Desktop/Haarcascade/teste/'+str(num_img)+'.jpg', img2)
    
    num_img +=1
    
    print(num_img)